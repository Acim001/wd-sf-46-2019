import {Link} from "react-router-dom";
import {useHistory} from "react-router";

const Course = ({course}) => {

    const history = useHistory()
    const toDetails  = () => {
        history.push(`/course/${course.id}`)
    }

    return(
            <div className="card col-md-4" style={{marginTop: 20}}  onClick={toDetails}>
                <img className="card-img-top" style={{height: 250, cursor: "pointer"}} src={course.slika} alt="Course picture"/>
                <div className="card-body" style={{cursor: "pointer"}}>
                    <h5 className="card-title">{course.naziv} <small className="text-muted">({course.prosecnaOcena})</small></h5>
                    <p className="card-text">{course.autor}</p>
                </div>
                <div className="card-footer">
                    <small className="text-muted">Last updated {course.datumIzmene}</small>
                </div>
            </div>
    )
}

export default Course