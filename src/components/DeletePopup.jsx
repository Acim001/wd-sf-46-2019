import Popup from "reactjs-popup";

const DeletePopup = ({title, type}) => {

    return(
        <Popup trigger={title === "Brisanje kursa" ?
            <button className="btn btn-danger col-md-3 offset-md-1">Obrisi</button>
            : <button className="btn btn-danger">Obrisi</button>} modal>
            {close => (
                <div className="card">
                    <div className="card-body">
                        <h5 className="card-title">{title}</h5>
                        <p className="text-dark">Da li ste sigurni?</p>
                        <br/>
                        <button className="btn btn-danger col-md-3 col-sm-12 offset-md-1" onClick={type}>Da</button>
                        <button className="btn btn-warning col-md-3 col-sm-12 offset-md-1" onClick={close}>Ne</button>
                    </div>
                </div>
            )}
        </Popup>
    )
}

export default DeletePopup