import Popup from "reactjs-popup";
import 'reactjs-popup/dist/index.css';
import {useState} from "react";

const Registration = () => {

    const [user, setUser] = useState({
        korisnickoIme: "",
        lozinka: "",
        email: "",
        ime: "",
        prezime: "",
        datumRodjenja: "",
        adresa: "",
        telefon: ""
    })
    const [repeatedPassword, setRepeatedPassword] = useState("")
    const pattern = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;

    const [warning, setWarning] = useState("")
    
    const validation = () => {
      if(!user.adresa || !user.ime || !user.datumRodjenja || !user.email || !user.korisnickoIme || !user.lozinka || !user.prezime || !user.telefon){
          setWarning("Sva polja moraju biti popunjena")
          return;
      }
      if(user.lozinka !== repeatedPassword){
          setWarning("Ponovljena lozinka i lozinka se ne poklapaju")
          return;
      }
      if(!user.email.match(pattern)){
          setWarning("Email nije ispravan")
          return;
      }

      setWarning("Uspesno ste se registrovali")
    }

    const handleFormInputChange = (name) => (e) => {
        const val = e.target.value
        setUser({...user, [name]: val})
    }

    return(
        <Popup trigger={<button className="btn btn-primary">Registruj se</button>} modal>
            {close => (
                <div className="card">
                    <div className="card-body">
                        <h5 className="card-title">Registracija</h5>
                        <>
                            <label className="form-label">Korisničko ime: </label>
                            <input className="form-control" onChange={handleFormInputChange("korisnickoIme")}/>
                            <div className="row">
                                <div className="col-md-6">
                                    <label className="form-label">Lozinka: </label>
                                    <input className="form-control" type="password" onChange={handleFormInputChange("lozinka")}/>
                                </div>
                                <div className="col-md-6">
                                    <label className="form-label">Ponovljena lozinka: </label>
                                    <input className="form-control" type="password" onChange={e => {setRepeatedPassword(e.target.value)}}/>
                                </div>
                            </div>

                            <label className="form-label">Email: </label>
                            <input className="form-control" type="email" onChange={handleFormInputChange("email")}/>
                            <div className="row">
                                <div className="col-md-6">
                                    <label className="form-label">Ime: </label>
                                    <input className="form-control" onChange={handleFormInputChange("ime")}/>
                                </div>
                                <div className="col-md-6">
                                    <label className="form-label">Prezime: </label>
                                    <input className="form-control" onChange={handleFormInputChange("prezime")}/>
                                </div>
                            </div>
                            <label className="form-label">Datum rođenja: </label>
                            <input className="form-control" type="date" onChange={handleFormInputChange("datumRodjenja")}/>
                            <label className="form-label">Adresa: </label>
                            <input className="form-control" onChange={handleFormInputChange("adresa")}/>
                            <label className="form-label">Broj telefona: </label>
                            <input className="form-control" onChange={handleFormInputChange("telefon")}/>
                            <br/>
                            <p className="text-danger">{warning}</p>
                            <br/>
                            <button className="btn btn-success col-md-3 col-sm-12" onClick={validation}>Registruj se</button>
                            <button className="btn btn-warning col-md-3 col-sm-12 offset-md-1" onClick={close}>Napusti</button>
                        </>
                    </div>
                </div>
            )}
        </Popup>
    )
}

export default Registration