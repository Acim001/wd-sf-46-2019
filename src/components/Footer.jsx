const Footer = () => {

    return(
        <footer className="footer mt-auto font-small bg-primary pt-4">
            <div className="container-fluid text-center text-md-left">
                <div className="row">
                    <div className="col-md-6 offset-md-3">
                        <h5 className="text-uppercase">Course Hub</h5>
                        <p>Best site for you</p>
                    </div>
                </div>
            </div>
            <div className="footer-copyright text-center py-3">© 2021 Copyright:
                <span> SF-46/2019</span>
            </div>
        </footer>
    )
}

export default Footer