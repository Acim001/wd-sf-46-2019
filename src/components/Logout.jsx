
const Logout = () => {

    const logout = () => {
        localStorage.clear()
        window.location.reload();
    }

    return(
        <button className="btn btn-primary" onClick={logout}>Odjava</button>
    )
}

export default Logout