import {db} from "../services/client/FirebaseService";
import {ref, remove} from "firebase/database";

const ItemInCart = ({course}) => {

    const removeCourse = () => {
        remove(ref(db, `korpa/${course.id}`))
    }

    return(
        <div className="col-md-3"  style={{marginTop: 20}}>
            <div className="card">
                <img className="card-img-top" style={{height: 200}} src={course.slika} alt="Course picture"/>
                <div className="card-body">
                    <h5 className="card-title">{course.naziv}</h5>
                    <p className="card-text">{course.cena}</p>
                </div>
                <button className="btn btn-warning" onClick={removeCourse}>Izbaci</button>
            </div>
        </div>
    )

}

export default ItemInCart