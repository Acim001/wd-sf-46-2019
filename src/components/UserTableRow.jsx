import {useHistory} from "react-router";
import DeletePopup from "./DeletePopup";
import {ref, remove} from "firebase/database";
import {db} from "../services/client/FirebaseService";

const UserTableRow = ({user, id}) => {

    const history = useHistory()
    const profile = () => {
        history.push(`/users/${id}`)
    }

    const deleteUser = () => {
        remove(ref(db, `korisnici/${id}`))
        history.push("/")
    }

    return(
        <tr>
            <td>{user.ime}</td>
            <td>{user.prezime}</td>
            <td>{user.korisnickoIme}</td>
            <td>{user.adresa}</td>
            <td>{user.telefon}</td>
            <td><button className="btn btn-info" onClick={profile}>Profil</button></td>
            <td><DeletePopup title={"Blokiranje korisnika"} type={deleteUser}/></td>
        </tr>
    )
}

export default UserTableRow