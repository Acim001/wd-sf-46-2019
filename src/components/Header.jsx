import {Link} from "react-router-dom";
import logo from "../images/logo.png"
import Login from "./Login";
import 'reactjs-popup/dist/index.css';
import Registration from "./Registration";
import Logout from "./Logout";
import {useEffect, useState} from "react";

const Header = () => {

    const [token, setToken] = useState()
    const headerType = () => {
      if(token === null){
          return <>
              <li className="nav-item"><Login/></li>
              <li className="nav-item" ><Registration/></li>
          </>
      }
      return <>
          <li className="nav-item"><Link className="nav-link text-white" to="/users">Korisnici</Link></li>
          <Logout/>
      </>
    }

    useEffect(()=> {
        setToken(localStorage.getItem("username"))
    })

    return(
        <header>
            <nav className="navbar navbar-expand-lg navbar-light bg-primary sticky-top">
                <div className="container">
                    <Link className="navbar-brand" to="/">
                        <img src={logo} alt="Logo" height="50px"/>
                    </Link>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse"
                            data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                            aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarResponsive">
                        <ul className="navbar-nav ms-auto">
                            <li className="nav-item">
                                <Link className="nav-link text-white" to="/">
                                    Pocetna
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link text-white" to="/cart">
                                    Korpa
                                </Link>
                            </li>
                            {headerType()}
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
    )
}

export default Header