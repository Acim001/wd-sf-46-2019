import Popup from "reactjs-popup";
import 'reactjs-popup/dist/index.css';
import {useEffect, useState} from "react";
import {db} from "../services/client/FirebaseService";
import { ref, onValue} from "firebase/database";

const Login = () => {

    const [username, setUsername] = useState("")
    const [password, setPassword] = useState("")
    const [message, setMessage] = useState("")

    const [users, setUsers] = useState([])
    useEffect(()=>{
        onValue(ref(db, "korisnici"), snapshot => {
            const data = snapshot.val()
            setUsers(Object.values(data))
        })
    },[])

    const login = () => {
        const ret = users.filter((user) => user.korisnickoIme === username)
        if(ret.length > 0) {
            const sds = Object.values(ret[0])
            if(sds[5] === password){
                localStorage.setItem("username", username)
                window.location.reload();
            }else{
                setMessage("Korisnicko ime ili lozinka nije tacno")
            }
        }else{
            setMessage("Korisnicko ime ili lozinka nije tacno")
        }
    }

    return (
        <Popup trigger={<button className="btn btn-primary">Prijavi se</button>} modal>
            {close => (
                <div className="card">
                    <div className="card-body">
                        <h5 className="card-title">Prijava</h5>
                        <>
                            <label className="form-label">Korisnicko ime: </label>
                            <input className="form-control" onChange={e => setUsername(e.target.value)}/>
                            <label className="form-label">Lozinka: </label>
                            <input className="form-control" type="password"
                                   onChange={e => setPassword(e.target.value)}/>
                            <br/>
                            <p className="text-danger">{message}</p>
                            <button className="btn btn-success col-md-3 col-sm-12" onClick={login}>Prijavi se</button>
                            <button className="btn btn-warning col-md-3 col-sm-12 offset-md-1" onClick={close}>Napusti</button>
                        </>
                    </div>
                </div>
            )}
        </Popup>
    )
}
export default Login