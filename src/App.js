import Header from "./components/Header";
import Footer from "./components/Footer";
import Courses from "./pages/Courses";
import CourseDetails from "./pages/CourseDetails";
import {Route, BrowserRouter as Router, Switch} from "react-router-dom";
import CourseEdit from "./pages/CourseEdit";
import Users from "./pages/Users";
import User from "./pages/User";
import UserEdit from "./pages/UserEdit";
import Cart from "./pages/Cart";

function App() {
  return (
    <div>
        <Router>
            <Header/>
            <div className="container">
                <Switch>
                    <Route exact path="/" component={Courses}/>
                    <Route exact path="/course/:id" component={CourseDetails}/>
                    <Route exact path="/edit-course/:id" component={CourseEdit}/>
                    <Route exact path="/users" component={Users}/>
                    <Route exact path="/users/:id" component={User}/>
                    <Route exact path="/edit-user/:id" component={UserEdit}/>
                    <Route exact path="/cart" component={Cart}/>
                </Switch>
            </div>
            <Footer/>
        </Router>
    </div>
  );
}

export default App;
