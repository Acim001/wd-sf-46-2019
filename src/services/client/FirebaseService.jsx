// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getDatabase } from "firebase/database"

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyC37HQ5HdXpam_lncKGCalmaIYhTQy_jP4",
    authDomain: "course-shop-db15a.firebaseapp.com",
    databaseURL: "https://course-shop-db15a-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "course-shop-db15a",
    storageBucket: "course-shop-db15a.appspot.com",
    messagingSenderId: "766350070030",
    appId: "1:766350070030:web:6e1c6a95215c50ea6f475f",
    measurementId: "G-P090ZM813M"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

export const db = getDatabase(app)

