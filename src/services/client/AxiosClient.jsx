import axios from "axios";

const url = process.env.REACT_APP_FIREBASE
const AxiosClient = axios.create({
        baseURL: url
    }
)

export default AxiosClient