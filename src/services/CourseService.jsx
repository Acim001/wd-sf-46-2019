import { db } from "./client/FirebaseService";
import { ref, set, onValue, remove, update } from "firebase/database";

const extractData = (data) => {
  console.log(Object.values(data))
}

const createCourse = (course) => {
    set(ref(db, "kursevi"), course)
}

const getAllCourses = () => {
    onValue(ref(db, "kursevi"), snapshot => {
        const data = snapshot.val()
        extractData(data)
        return Object.values(data)
    })
}

const getOneCourse = (courseId) => {
  onValue(ref(db, `kursevi/${courseId}`), snapshot => {
      const data = snapshot.val()
      return data
  })
}

const updateCourse = (courseId, course) => {
    update(ref(db, `kursevi/${courseId}`), course)
}

const deleteCourse = (courseId) => {
    remove(ref(db, `kursevi/${courseId}`))
}

export const CourseService = {
    createCourse,
    getAllCourses,
    getOneCourse,
    updateCourse,
    deleteCourse
}