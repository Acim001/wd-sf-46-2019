import UserTableRow from "../components/UserTableRow";
import {useHistory} from "react-router";
import {useEffect, useState} from "react";
import {onValue, ref} from "firebase/database";
import {db} from "../services/client/FirebaseService";

const Users = () => {

    const [users, setUsers] = useState([])
    const [keys, setKeys] = useState([])
    useEffect(()=>{
        onValue(ref(db, "korisnici"), snapshot => {
            const data = snapshot.val()
            setKeys(Object.keys(data))
            setUsers(Object.values(data))
        })
    },[])

    return(
        <div className="row" style={{marginTop: 20, marginBottom: 173}}>
            <div className="table-responsive-md">
                <table className="table">
                    <thead className="table-primary">
                        <tr>
                            <th>Ime</th>
                            <th>Prezime</th>
                            <th>Korisničko ime</th>
                            <th>Adresa</th>
                            <th>Broj telefona</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {users.map((user, index) => (
                            <UserTableRow user={user} key={index} id={keys[index]}/>
                        ))}
                    </tbody>
                </table>
            </div>
        </div>
    )
}

export default Users