import {useHistory, useParams} from "react-router";
import {useEffect, useState} from "react";
import {onValue, ref, remove, set} from "firebase/database";
import {db} from "../services/client/FirebaseService";
import DeletePopup from "../components/DeletePopup";

const CourseDetails = () => {

    const username = localStorage.getItem("username")
    const [course, setCourse] = useState({})
    const history = useHistory()
    const {id} = useParams()
    const edit = () => history.push(`/edit-course/${id}`)
    const courseToCart = {
        autor: course.autor,
        cena : course.cena,
        id : course.id,
        jezik : course.jezik,
        kategorija : course.kategorija,
        naziv : course.naziv,
        slika: course.slika
    }

    const deleteCourse = () => {
        remove(ref(db, `kursevi/${course.id}`))
        history.push("/")
    }


    const putInCart = () => {
        set(ref(db, `korpa/${course.id}`), courseToCart)
        history.push("/cart")
    }

    useEffect(() => {
        onValue(ref(db, `kursevi/${id}`), snapshot => {
            const data = snapshot.val()
            setCourse(data)
        })
    },[])

    return(
        <div className="row" style={{marginBottom: 60, marginTop: 20}}>
            <div className="col-md-4 col-xs-12">
                <img width="600px" height="400px" alt="Course image" src={course.slika}/>
            </div>
            <div className="col-md-6 col-xs-12 offset-md-2">
                <h2 className="font-weight-bold">{course.naziv}</h2>
                <p className="text-muted">{course.autor}</p>
                <p>Poslednji datum izmene: <span className="text-muted">{course.datumIzmene}</span></p>
                <p>Opis: <span className="text-muted">{course.opis}</span></p>
                <p>Cena: <span className="text-muted">{course.cena}</span></p>
                <p>Broj lekcija: <span className="text-muted">{course.brojLekcija}</span></p>
                <p>Kategorija: <span className="text-muted">{course.kategorija}</span></p>
                <p>Jezik: <span className="text-muted">{course.jezik}</span></p>
                <p>Prosecna ocena: <span className="text-muted">{course.prosecnaOcena}</span></p>
                <p>Broj korsinika: <span className="text-muted">{course.brojKorisnika}</span></p>
                <p>Sertifikovan: <span className="text-muted">{course.sertifikovan}</span></p>
                {
                    username !== null ?
                    <div className="row">
                        <button className="btn btn-warning col-md-3" onClick={edit}>Izmeni</button>
                        <DeletePopup title={"Brisanje kursa"} type={deleteCourse}/>
                        <button className="btn btn-info col-md-3 offset-md-1" onClick={putInCart}>Kupi</button>
                    </div> : ""
                }

            </div>
        </div>
    )
}
export default CourseDetails