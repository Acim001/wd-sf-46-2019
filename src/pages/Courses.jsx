import Course from "../components/Course"
import {useEffect, useState} from "react";
import {onValue, ref} from "firebase/database";
import {db} from "../services/client/FirebaseService";

const Courses = () => {

    const [courses, setCourses] = useState([])

    useEffect(()=>{
        onValue(ref(db, "kursevi"), snapshot => {
            const data = snapshot.val()
            setCourses(Object.values(data))
        })
    },[])

    return(

        <div className="card-deck">
            <div className="row">
                {courses.map((course, index) =>
                    (<Course key={index} course={course}/> ))}
            </div>
        </div>
    )
}
export default Courses