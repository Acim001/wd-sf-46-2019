import {useHistory, useParams} from "react-router";
import {onValue, ref, set} from "firebase/database";
import {db} from "../services/client/FirebaseService";
import {useEffect, useState} from "react";

const CourseEdit = () => {

    const [course, setCourse] = useState({})
    const {id} = useParams()
    const history = useHistory()

    const edit = () => {
        set(ref(db, `kursevi/${course.id}`), course)
        history.push(`/course/${course.id}`)
    }

    useEffect(() => {
        onValue(ref(db, `kursevi/${id}`), snapshot => {
            const data = snapshot.val()
            setCourse(data)
        })
    },[])

    const handleFormInputChange = (name) => (e) => {
        const val = e.target.value
        setCourse({...course, [name]: val})
    }

    return(
        <div className="row">
            <h2 className="text-center">Izmena kursa</h2>
            <div className="col-md-6 offset-md-3 card" style={{marginBottom: 20}}>
                <>
                    <label className="form-label">ID: </label>
                    <input className="form-control" disabled value={course.id}/>
                    <label className="form-label">Naziv: </label>
                    <input className="form-control" value={course.naziv} onChange={handleFormInputChange("naziv")}/>
                    <label className="form-label">Autor: </label>
                    <input className="form-control" value={course.autor} onChange={handleFormInputChange("autor")}/>
                    <label className="form-label">Datum poslednje izmene: </label>
                    <input className="form-control" type="date" value={course.datumIzmene} onChange={handleFormInputChange("datumIzmene")}/>
                    <label className="form-label">Opis: </label>
                    <textarea rows="5" className="form-control" value={course.opis} onChange={handleFormInputChange("opis")}/>
                    <label className="form-label">Cena: </label>
                    <input className="form-control" type="number" value={course.cena} onChange={handleFormInputChange("cena")}/>
                    <label className="form-label">Broj lekcija: </label>
                    <input className="form-control" type="number" value={course.brojLekcija} onChange={handleFormInputChange("brojLekcija")}/>
                    <label className="form-label">Kategorija: </label>
                    <select className="form-select">
                        <option value="web">Web programiranje</option>
                        <option selected value="objektno">Objektno programiranje</option>
                        <option value="algoritmi">Algoritmi</option>
                        <option value="strukture">Strukture podataka</option>
                        <option value="baze">Baze podataka</option>
                    </select>
                    <label className="form-label">Jezik: </label>
                    <input className="form-control" value={course.jezik} onChange={handleFormInputChange("naziv")}/>
                    <label className="form-label">Prosečna ocena: </label>
                    <input className="form-control" disabled value={course.prosecnaOcena}/>
                    <label className="form-label">Broj korisnika: </label>
                    <input className="form-control" disabled value={course.brojKorisnika}/>
                    <div className="form-check">
                        <label className="form-check-label">Sertifikovan </label>
                        {course.sertifikovan === "da" ?
                            <input className="form-check-input" checked type="checkbox"/>
                            :
                            <input className="form-check-input" type="checkbox"/>}
                    </div>
                    <br/>
                    <button className="btn btn-success col-md-12" onClick={edit}>Izmeni</button>
                </>
                <br/>
            </div>
        </div>
    )
}

export default CourseEdit