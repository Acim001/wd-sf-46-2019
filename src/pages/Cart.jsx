import ItemInCart from "../components/ItemInCart";
import {useEffect, useState} from "react";
import {onValue, ref, remove} from "firebase/database";
import {db} from "../services/client/FirebaseService";

const Cart = () => {

    const [cart, setCart] = useState([])
    const [cena, setCena] = useState(0)

    useEffect(()=>{
        onValue(ref(db, "korpa"), snapshot => {
            const data = snapshot.val()
            if(data !== null){
                setCart(Object.values(data))
            }else{
                setCart([])
            }
        })
    },[])

    useEffect(()=>{
        cenaVal()
    }, [cart])

    const cenaVal = () => {
        let ukupnaCena = 0
        cart.forEach(item => ukupnaCena = ukupnaCena + item.cena)
        setCena(ukupnaCena)
    }

    const buy = () => {
        cart.forEach(item => remove(ref(db, `korpa/${item.id}`)))
    }

    return(
        <div className="row" style={{}}>
            {cart.length > 0 ? cart.map((course, index) => (
                <ItemInCart course={course} key={index}/>
            )) : <h1 className="text-center" style={{marginTop: 270, marginBottom: 208}}>Trenutno nema artikala u korpi</h1>}
            {cart.length > 0 ?
                <div className="row" style={{marginTop: 200, marginBottom: 5}}>
                    <p className="text-start fs-1 col-md-6">Ukupna cena: {cena} </p>
                    <button className="btn btn-success col-md-3 offset-md-3" onClick={buy}>Kupi</button>
                </div>
                : ""}
        </div>
    )

}

export default Cart
