import {useHistory, useParams} from "react-router";
import {useEffect, useState} from "react";
import {onValue, ref, set} from "firebase/database";
import {db} from "../services/client/FirebaseService";

const UserEdit = () => {

    const [user, setUser] = useState({})
    const {id} = useParams()
    const history = useHistory()
    const [warning, setWarning] = useState("")

    const edit = () => {
        set(ref(db, `korisnici/${id}`), user)
        history.push(`/users/${id}`)
    }

    useEffect(() => {
        onValue(ref(db, `korisnici/${id}`), snapshot => {
            const data = snapshot.val()
            setUser(data)
        })
    },[])

    const handleFormInputChange = (name) => (e) => {
        const val = e.target.value
        setUser({...user, [name]: val})
    }

    const validator = () => {
      if(!user.email || !user.ime || !user.prezime || !user.korisnickoIme || !user.adresa || !user.telefon){
          setWarning("Sva polja moraju biti popunjena")
          return;
      }
      edit()
    }

    return(
        <div className="row">
            <h2 className="text-center">Izmena podataka o korisniku</h2>
            <div className="col-md-6 offset-md-3 card" style={{marginBottom: 20}}>
                <>
                    <label className="form-label">Ime: </label>
                    <input type="text" className="form-control" value={user.ime} onChange={handleFormInputChange("ime")}/>
                    <label className="form-label">Prezime: </label>
                    <input className="form-control" value={user.prezime} onChange={handleFormInputChange("prezime")}/>
                    <label className="form-label">Korisničko ime: </label>
                    <input className="form-control" value={user.korisnickoIme} onChange={handleFormInputChange("korisnickoIme")}/>
                    <label className="form-label">Adresa: </label>
                    <input className="form-control" value={user.adresa} onChange={handleFormInputChange("adresa")}/>
                    <label className="form-label">Email: </label>
                    <input type="email" className="form-control" value={user.email} onChange={handleFormInputChange("email")}/>
                    <label className="form-label">Broj telefona: </label>
                    <input className="form-control" value={user.telefon} onChange={handleFormInputChange("telefon")}/>
                    <label className="form-label">Datum rođenja: </label>
                    <input type="date" className="form-control" value={user.datumRodjenja} onChange={handleFormInputChange("datumRodjenja")}/>
                    <br/>
                    <p className="text-danger">{warning}</p>
                    <br/>
                    <button className="btn btn-success col-md-12" onClick={validator}>Izmeni</button>
                </>
                <br/>
            </div>
        </div>
    )
}

export default UserEdit