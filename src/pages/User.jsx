import {useHistory, useParams} from "react-router";
import {useEffect, useState} from "react";
import {onValue, ref} from "firebase/database";
import {db} from "../services/client/FirebaseService";

const User = () => {

    const [user, setUser] = useState({})
    const history = useHistory()
    const {id} = useParams()
    const edit = () => history.push(`/edit-user/${id}`)

    useEffect(() => {
        onValue(ref(db, `korisnici/${id}`), snapshot => {
            const data = snapshot.val()
            setUser(data)
        })
    },[])
    return(
        <div className="row">
            <h2 className="text-center">Podaci o korisniku</h2>
            <div className="col-md-6 offset-md-3 card" style={{marginBottom: 20}}>
                <form>
                    <label className="form-label">Ime: </label>
                    <p className="lead">{user.ime}</p>
                    <label className="form-label">Prezime: </label>
                    <p className="lead">{user.prezime}</p>
                    <label className="form-label">Korisničko ime: </label>
                    <p className="lead">{user.korisnickoIme}</p>
                    <label className="form-label">Adresa: </label>
                    <p className="lead">{user.adresa}</p>
                    <label className="form-label">Email: </label>
                    <p className="lead">{user.email}</p>
                    <label className="form-label">Broj telefona: </label>
                    <p className="lead">{user.telefon}</p>
                    <label className="form-label">Datum rođenja: </label>
                    <p className="lead">{user.datumRodjenja}</p>
                    <br/>
                    <button type="submit" className="btn btn-success col-md-12" onClick={edit}>Izmeni</button>
                </form>
                <br/>
            </div>
        </div>
    )
}

export default User